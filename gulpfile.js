'use strict';
var gulp = require('gulp');
var jade = require('gulp-jade');
var plumber = require('gulp-plumber');
var notify = require("gulp-notify");
var sass = require("gulp-sass");
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');

// Browser definitions for autoprefixer
var AUTOPREFIXER_BROWSERS = [
  'last 3 versions',
  'ie >= 9',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];

gulp.task('scss', function () {
  gulp.src('scss/**/*.scss')
    // .pipe(sourcemaps.init())
      .pipe(sass({outputStyle: 'compact'}).on('error', sass.logError))
      .pipe(autoprefixer({
            browsers: AUTOPREFIXER_BROWSERS,
            cascade: false
      }))
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest('css'));

});
gulp.task('setPrefixes', function () {
    return gulp.src('css/**/*.css')
    // .pipe(sourcemaps.init())
        .pipe(autoprefixer({
            browsers: AUTOPREFIXER_BROWSERS,
            cascade: false
        }))
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest('css'));
});

gulp.task('pages', function() {
  var YOUR_LOCALS = {};
 
 return gulp.src('jade/pages/*.jade')
 	.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(jade({
      locals: YOUR_LOCALS,
      pretty:true
    }))
    .pipe(gulp.dest('./'))
    .pipe(notify({
    	title:"Sometitlie",
    	message:"Jade file <%= file.relative %> was complied",
    	
    }))

});

gulp.task('default',function(){
  gulp.watch('jade/**/*.jade',['pages']);
  gulp.watch('scss/**/*.scss',['scss']);
	// gulp.watch('css/**/*.css',['setPrefixes']);
});
